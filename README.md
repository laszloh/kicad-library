# Private KiCad Library

> Notice: This library is compliant to the [KLC](https://klc.kicad.org/).

This repository contains the Symbols, Footprint, and 3D Models used for my private projects.

> Notice: The libraries are provided in the hope that they will be useful but without a warranty of any kind.

**The libraries in this repository are intended to be used with KiCad version 8.**

For the KiCad legacy libraries, please use the old branches:

* [KiCad 6](https://gitlab.com/laszloh/kicad-library/-/tree/KiCAD-6.x)
* [KiCad 7](https://gitlab.com/laszloh/kicad-library/-/tree/KiCAD-7.x)

Each footprint library is stored as a directory with the .pretty suffix. The footprint files are .kicad_mod files within.


## Symbols and Footprints

All footprints were designed according to the Recommended PCB Land Pattern section present on each module datasheet.

### Parts

The following parts are included in this library

**Not filles out yet**

| Part              | Symbol | Footprint | Resource                                                                                               |
|:------------------|:------:|:---------:|:-------------------------------------------------------------------------------------------------------|

### Modules

| Module           | Symbol | Footprint | Resource                                                                                                                    |
|:-----------------|:------:|:---------:|:----------------------------------------------------------------------------------------------------------------------------|


## Manual Installation - PCM

The Espressif KiCad library is distributed via the Pluguin and Content Manager (PCM) and the installation is done automatically.

To install the library, you need to download the **[KiCad-8.x CI artifact](https://gitlab.com/api/v4/projects/55480958/jobs/artifacts/KiCad-8.x/raw/kicad_library.zip?job=compress)** file or the latest release.

> Make sure to download the correct zip file and ***do not extract the files***. If you are using macOS and Safari, ensure to that the automatic unzip feature (**Open safe files after downloading**) is disabled.

## Contributing

If you want to contribute, please consider sending creating a Pull Request (PR).

### About KiCad

KiCad is a Cross-Platform and Open Source Electronics Design Automation Suite. See [KiCad EDA](https://kicad.org/) for more information.